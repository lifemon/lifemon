/************************************/
// This file contains everything
// necessary for managing datas
// used by the application.
/************************************/
package main

import (
    "encoding/json"
//    "lifemon/utils"
//    "fmt"
)

/************************************/
// ELEMENT
//
// This structure contains the
// definition of a specific Element:
// - type : determines how element is displayed
// - slug : unique identifier of this element
// - label : name of this element
// - description : description of this element
// - unit : unit to use for this element (Optional)
// - cmin : minimal value of this element (Optional)
// - cmax : maximal value of this element (Optional)
// - values : list of Element (Optional)
/************************************/
type Element struct {
    Type string `json:"type"`
    Slug string `json:"slug"`
    Label string `json:"label"`
    Description string `json:"description"`
    Unit string `json:"unit"`
    Min string `json:"cmin"`
    Max string `json:"cmax"`
    Values []Element `json:"values"`
}

// Returns True if element is of type "quantity"
func (element *Element) IsQuantity() bool {
    if (element.Type=="quantity") {
        return true
    }
    return false
}

// Returns True if element is of type "string"
func (element *Element) IsString() bool {
    if (element.Type=="string") {
        return true
    }
    return false
}

// Returns True if element is of type "choices"
func (element *Element) IsChoices() bool {
    if (element.Type=="choices") {
        return true
    }
    return false
}

// Returns True if element is of type "cursor"
func (element *Element) IsCursor() bool {
    if (element.Type=="cursor") {
        return true
    }
    return false
}

/************************************/
// INDICATOR
//
// This structure contains the
// definition of a specific Indicator:
// - name : name of this indicator
// - slug : unique identifier of this indicator
// - description : description of this indicator
// - category : category of this indicator
// - elements : list of Element
/************************************/
type Indicator struct {
    Name string `json:"name"`
    Slug string `json:"slug"`
    Description string `json:"description"`
    Category string `json:"category"`
    Elements []Element `json:"elements"`
}

/************************************/
// INDICATORS
//
// This structure contains the
// definition of the list of available
// Indicators:
// - indicators : list of Indicator
/************************************/
type Indicators struct {
    List []Indicator `json:"indicators"`
}

// Reading indicators structure from file
func (indicator *Indicators) Read(filename string) {
    json.Unmarshal(ReadDatas(filename, ""), &indicator)
    for i, value := range indicator.List {
        indicator.List[i].Name = translate(value.Name)
        indicator.List[i].Description = translate(value.Description)
        for j, val := range indicator.List[i].Elements {
            indicator.List[i].Elements[j].Label = translate(val.Label)
            indicator.List[i].Elements[j].Description = translate(val.Description)
            if (val.Type=="choices") {
                for k, choice := range indicator.List[i].Elements[j].Values {
                    indicator.List[i].Elements[j].Values[k].Label = translate(choice.Label)
                }
            }
        }
    }
}

// Writing indicators structure in file
func (indicator Indicators) Write(filename string) {
    marshaled, err := json.Marshal(indicator)
    if err != nil {
        panic(err)
    }
    SaveDatas(marshaled, filename, "")
}

/************************************/
// CATEGORY
//
// This structure contains the
// definition of a specific Category:
// - name : name of this category
// - slug : unique identifier of this category
/************************************/
type Category struct {
    Name string `json:"name"`
    Slug string `json:"slug"`
}

/************************************/
// CATEGORIES
//
// This structure contains the
// definition of the list of available
// Categories:
// - categories : list of Category
/************************************/
type Categories struct {
    List []Category `json:"categories"`
}

// Reading categories structure from file
func (category *Categories) Read(filename string) {
    json.Unmarshal(ReadDatas(filename, ""), &category)
    for i, value := range category.List {
            category.List[i].Name = translate(value.Name)
        }
}

// Writing categories structure in file
func (category Categories) Write(filename string) {
    marshaled, err := json.Marshal(category)
    if err != nil {
        panic(err)
    }
    SaveDatas(marshaled, filename, "")
}

/************************************/
// INDICATOR FILE
//
// This structure contains the
// definition of an Indicator File:
// - slug : unique identifier of this indicator file
// - filename : filename used to store datas of a specific indicator
/************************************/
type IndicatorFile struct {
    Slug string `json:"slug"`
    Filename string `json:"filename"`
}

/************************************/
// SETTINGS
//
// This structure contains the
// list of indicators files used by
// the application:
// - indicators : list of IndicatorFile
/************************************/
type Settings struct {
    List []IndicatorFile `json:"indicators"`
}

// Reading settings structure from file
func (settings *Settings) Read(filename string, key string) {
    //TODO: Change this to read encrypted datas
    //json.Unmarshal(ReadDatas(filename, key), &settings)
    json.Unmarshal(ReadDatas(filename, ""), &settings)
}

// Writing settings structure in file
func (settings Settings) Write(filename string, key string) {
    marshaled, err := json.Marshal(settings)
    if err != nil {
        panic(err)
    }
    //TODO: Change this to save encrypted datas
    //SaveDatas(marshaled, filename, key)
    SaveDatas(marshaled, filename, "")
}

/************************************/
// VALUE
//
// This structure contains the
// definition of an Value:
// - slug : unique identifier of this value
// - value : effective value of this value
/************************************/
type Value struct {
    Slug string `json:"slug"`
    Value string `json:"value"`
}

/************************************/
// DATA
//
// This structure contains the
// definition of a Data:
// - timestamp : date and time when data is added
// - notes : text used to take note
// - values : list of Value for this specific Data
/************************************/
type Data struct {
    Timestamp string `json:"timestamp"`
    Notes string `json:"notes"`
    Values []Value `json:"values"`
}

/************************************/
// DATAS
//
// This structure contains the list
// of listed Data:
// - datas : list of Data
/************************************/
type Datas struct {
    Datas []Data `json:"data"`
}

// Reading datas structure from file
func (datas *Datas) Read(filename string, key string) {
    //TODO: Change this to read encrypted datas
    //json.Unmarshal(ReadDatas(filename, key), &datas)
    json.Unmarshal(ReadDatas(filename, ""), &datas)
}

// Writing datas structure in file
func (datas Datas) Write(filename string, key string) {
    marshaled, err := json.Marshal(datas)
    if err != nil {
        panic(err)
    }
    //TODO: Change this to save encrypted datas
    //SaveDatas(marshaled, filename, key)
    SaveDatas(marshaled, filename, "")
}

/************************************/
// PUBLIC DATAS
//
// This structure contains the list
// of Public Data:
// - datas : list of Data
/************************************/
type PublicDatas struct {
    Datas []Data `json:"data"`
}

// Reading public datas structure from file
func (datas *PublicDatas) Read(filename string, key string) {
    json.Unmarshal(ReadDatas(filename, ""), &datas)
}

// Writing public datas structure in file
func (datas PublicDatas) Write(filename string, key string) {
    marshaled, err := json.Marshal(datas)
    if err != nil {
        panic(err)
    }
    SaveDatas(marshaled, filename, "")
}
