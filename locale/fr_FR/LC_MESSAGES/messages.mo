��    (      \  5   �      p  	   q     {     �  	   �     �  
   �     �  ]   �                 q   -     �     �     �     �     �     �     �          %      9     Z     q     �     �     �     �     �          !     6     M  $   c     �     �     �     �     �  E  �     C     Y     `     h     x          �  y   �     	     	      	  n   :	     �	     �	     �	     �	     �	     �	     �	  �   �	     q
     y
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
  w            �     �     �     �                              !                  '   &          #                     %                     
            "                (          $                 	                  Add value Back Cancel Dashboard Date : Disconnect Filter It seems there is no data yet. You can then create one account by simply entering a password. Notes Password Personal informations Please take some time to choose a good password. If you can't remember it, your datas won't be available anymore. Save Settings Submit category_name_health category_name_medical category_name_physiology category_name_sport element_description_pain element_label_color element_label_diastolic_pressure element_label_distance element_label_dose element_label_duration element_label_medication element_label_pain element_label_sleeping_duration element_label_systolic_pressure element_value_blood_stain element_value_liquid element_value_parasite element_value_residue indicator_description_blood_pressure indicator_name_blood_pressure indicator_name_faeces indicator_name_run indicator_name_sleep indicator_name_treatment Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-28 18:23+0100
Last-Translator: Mindiell <mindiell@mindiell.net>
Language-Team: none
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.2.1
 Ajouter un indicateur Retour Annuler Tableau de bord Date : Déconnexion Filtre Il semble qu'il n'y a pas encore de données. Vous pouvez créer un nouveau compte en entrant simplement un mot de passe. Notes Mot de passe Informations personnelles Merci de bien réfléchir avant de choisir votre mot de passe. Si vous l'oubliez, vos données seront perdues. Sauver Paramètres Valider Santé Médical Physiologie Sport La douleur s'évalue sur une échelle de 1 à 10. 1 correspond à 'pas de douleur' et 10 correspond à 'la douleur la plus insupportable'. Couleur Pression diastolique Distance Dose Durée Médicament Douleur Durée du sommeil Pression systolique Traces de sang Liquide Parasites Résidus La pression artérielle, ou pression artérielle systémique, correspond à la pression du sang dans les artères de la circulation systémique (circulation principale). La pression systolique (PAS) est la pression maximale, au moment de la contraction du cœur (systole). La pression diastolique (PAD) est la pression minimale, au moment du relâchement du cœur (diastole). Pression artérielle Selles Course à pied Sommeil Prise de médicament 