/************************************/
// This file contains everything
// which can't be in the other files
// but is used by the application.
/************************************/
// TODO: Add some logs...
package main

import (
    "crypto/aes"
    "crypto/cipher"
    "crypto/rand"
    "crypto/sha256"
//    "encoding/json"
    "errors"
    "io"
    "io/ioutil"
//    "fmt"
//    "log"
    math_rand "math/rand"
    "os"
)

/*
This function tries to open a file
If this is done without error
    - return true
Else if the error is "Not exists"
    - return false
Else
    - go into panic
*/
func IsExists(filename string) (bool) {
    _, err := os.Open(filename)
    if err != nil {
	    if os.IsNotExist(err) {
	        return false
	    }
    	panic(err)
    }
    return true
}

/*
This function reads data from a file using crypto
*/
func ReadDatas(filename string, key string) ([]byte) {
    // Read datas from a file to an indicator
    data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	// Load data
    var decrypted []byte = data
	// And decrypt only if key is not empty
	if key != "" {
	    decrypted, err = Decrypt(data, []byte(key))
        if err != nil {
            panic(err)
        }
    }
    return decrypted
}

/*
This function saves data to a file using crypto
*/
func SaveDatas(datas []byte, filename string, key string) {
	var err error
	// Set data
	var encrypted []byte = datas
	// Encrypt only if key is not empty
	if key != "" {
        encrypted, err = Encrypt(datas, []byte(key))
        if err != nil {
            panic(err)
        }
    }
    // Write to file
    ioutil.WriteFile(filename, encrypted, 0644)
}

/*
This function encrypts data using a key and some crypto
*/
func Encrypt(source []byte, key []byte) ([]byte, error) {
    // Converting key to accepted size bytes
    key_sha := sha256.Sum256(key)
    key_byte := make([]byte, len(key_sha))
    for i := range key_sha {
        key_byte[i] = key_sha[i]
    }
    // Creating block for AES
	block, err := aes.NewCipher(key_byte)
	if err != nil {
		panic(err)
	}
	// Using AES-CGM algorithm
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err)
	}
	// Creating random vector
	nonce := make([]byte, gcm.NonceSize())
    if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
        return nil, err
    }
    // Encrypt
    return gcm.Seal(nonce, nonce, source, nil), nil
}

/*
This function decrypts data using a key and some crypto
*/
func Decrypt(source []byte, key []byte) ([]byte, error) {
    // Converting key to accepted size bytes
    key_sha := sha256.Sum256(key)
    key_byte := make([]byte, len(key_sha))
    for i := range key_sha {
        key_byte[i] = key_sha[i]
    }
    // Creating block for AES
	block, err := aes.NewCipher(key_byte)
	if err != nil {
		panic(err)
	}
	// Using AES-CGM algorithm
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err)
	}
    nonceSize := gcm.NonceSize()
    if len(source) < nonceSize {
        return nil, errors.New("ciphertext too short")
    }
    nonce, source := source[:nonceSize], source[nonceSize:]
    // Decrypt
    return gcm.Open(nil, nonce, source, nil)
}

/*
This function returns a random string of size n
*/
func randString(n int) string {
    var letters = []rune("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    b := make([]rune, n)
    for i := range b {
        // TODO: Not real random ?
        b[i] = letters[math_rand.Intn(len(letters))]
    }
    return string(b)
}
