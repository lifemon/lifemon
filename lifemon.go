/************************************/
// This file contains everything
// necessary for launching the
// webserver used to serve
// the application.
/************************************/
package main

import (
    "net/http"
)

// Main funtion, launching local webserver
func main() {
    http.ListenAndServe(":8080", prepareHandlers())
}
