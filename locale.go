/************************************/
// This file contains everything
// necessary for managing locales
// used by the application.
/************************************/
package main

import (
    "github.com/chai2010/gettext-go/gettext"
)

// Setup the locale to use and where strings are stored
func setup(locale string, domain string, dir string) {
	gettext.SetLocale(locale)
	gettext.Textdomain(domain)

	gettext.BindTextdomain(domain, dir, nil)
}

// Change the locale to use
func changeLocale(locale string) {
	gettext.SetLocale(locale)
}

// Translate a string based on locale
func translate(input string) string {
	return gettext.PGettext("", input)
}
