/************************************/
// This file contains everything
// necessary for managing the web
// interface of application.
/************************************/
package main

import (
    "html/template"
    "net/http"
    "time"
    "fmt"
    "github.com/gorilla/mux"
)

// Functions mapping used inside templates
var funcMap = template.FuncMap{
	"gt": translate,
}
// Template files are parsed here for once when launching application
var templates = template.Must(template.ParseFiles(
    "templates/header.html",
    "templates/footer.html",
    "templates/filter.html",
    "templates/home.html",
    "templates/categories.html",
    "templates/category.html",
    "templates/menu.html",
    "templates/parameters.html",
    "templates/personal.html",
    "templates/dashboard.html",
    "templates/public.html",
    "templates/login.html",
    "templates/indicator.html")).Funcs(funcMap)
// Password used by user to encrypt and decrypt her files
var password = ""


/************************************/
// Web handlers
/************************************/

/*
This function is the handler of the homepage.
If password is already set
    - it redirects user to the categories page
Else, if file main.json exists
    - it redirects user to the login page
Else
    - it displays home page
*/
func homeHandler(w http.ResponseWriter, r *http.Request) {
    // If password has been set, user is redirected to categories
    if len(password)>0 {
        http.Redirect(w, r, "/category", 303)
    }
    // If file main.json exists, user is redirected to login
    if (IsExists("./datas/main.json")) {
        http.Redirect(w, r, "/login", 303)
    }
    // No file, let's create one
    renderTemplate(w, "home", nil)
}

/*
This function handles the creation of a new pack of datas.
This is done only once when user sets her password for first time.
The main.json file is created and user is redirected to the menu page.
*/
func createHandler(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    // Setting actual password only if necessary
    if len(r.PostForm)>0 {
        password = r.PostFormValue("password")
    } else {
        http.Redirect(w, r, "/", 303)
    }
    // Create main.json file
    var settings Settings
    settings.Write("./datas/main.json", password)
    // Everything is ok, let's redirect to main menu
    http.Redirect(w, r, "/menu", 303)
}

/*
This function is the handler of the login POST request.
If file main.json is not existing
    - it redirects user to the home page
If password is already set
    - it redirects user to the home page
If form contains a password
    - it fill password with it
    - it redirects user to the categories page
*/
func loginHandler(w http.ResponseWriter, r *http.Request) {
    // If file main.json does not exists, user should enter its password
    if (!IsExists("./datas/main.json")) {
        http.Redirect(w, r, "/", 303)
    }
    // If password is already set, redirect to homepage
    if len(password)>0 {
        http.Redirect(w, r, "/", 303)
    }
    r.ParseForm()
    // Setting actual password only if necessary
    if len(r.PostForm)>0 {
        password = r.PostFormValue("password")
        http.Redirect(w, r, "/menu", 303)
    }
    renderTemplate(w, "login", nil)
}

/*
This function is the handler of the logout request.
Reset password and redirect to homepage.
*/
func logoutHandler(w http.ResponseWriter, r *http.Request) {
    password = ""
    http.Redirect(w, r, "/", 303)
}

/*
This function is the handler of the menu page.
Simply display the menu template.
*/
func menuHandler(w http.ResponseWriter, r *http.Request) {
    verifyPassword(w,r)
    // Display menu
    renderTemplate(w, "menu", nil)
}

/*
This function is the handler of the parameters page.
Simply display the parameters template.
*/
func parametersHandler(w http.ResponseWriter, r *http.Request) {
    verifyPassword(w,r)
    // Display parameters
    renderTemplate(w, "parameters", nil)
}

/*
This function is the handler of the personal page.
It manages all the personal informations.
*/
func personalHandler(w http.ResponseWriter, r *http.Request) {
    verifyPassword(w,r)
    // Display personal
    renderTemplate(w, "personal", nil)
}

/*
This function is the handler of the dashboard page.
Simply display the dashboard template.
*/
func dashboardHandler(w http.ResponseWriter, r *http.Request) {
    verifyPassword(w,r)
    // Display dashboard
    renderTemplate(w, "dashboard", nil)
}

/*
This function is the handler of the public page.
Simply display the public template after loading public datas.
*/
func publicHandler(w http.ResponseWriter, r *http.Request) {
    // Create context struct
    context := struct {
    } {
    }
    // Display public
    renderTemplate(w, "public", context)
}

/*
This function is the handler of the categories page.
If a category has been selected
    - it lists indicators of selected category
Else
    - it lists all categories
*/
func categoryHandler(w http.ResponseWriter, r *http.Request) {
    verifyPassword(w,r)
    // Get parameter category's value
    category := mux.Vars(r)["category"]

    // If a category has been selected
    if (category!="") {
        // TODO : filter via GET method
        /*
        filter := ""
        if (r.Method=="POST") {
            r.ParseForm()
            filter = r.PostFormValue("filter")
        }
        */
        // Select only indicators from selected category
        // By reading indicators from file
        var indics Indicators
        indics.Read("./datas/indicators.json")
        var indicators Indicators
        for _, value := range indics.List {
            // TODO : filter indicators if filter is not empty
            // And selecting indicators linked to this category
            if (value.Category == category) {
                indicators.List = append(indicators.List, value)
            }
        }
        // Create context struct
        context := struct {
            Category string
            Indicators Indicators
        } {
            category,
            indicators,
        }
        // Display indicators
        renderTemplate(w, "category", context)

    // If no category has been choosen
    } else {
        // Read categories from file
        var categories Categories
        categories.Read("./datas/categories.json")
        // Create context struct
        context := struct {
            Categories Categories
        } {
            categories,
        }
        // Display categories
        renderTemplate(w, "categories", context)
    }
}

/*
This function is the handler of the indicator page.
If POST has been used
    - it search for corresponding file
    - if file is not found
        - it creates it
    - else
        - it uses it
    - it appends new data to the end of the indicator file if necessary
- it displays indicator page
*/
func indicatorHandler(w http.ResponseWriter, r *http.Request) {
    verifyPassword(w,r)
    // Get parameter indicator's value
    indicator := mux.Vars(r)["indicator"]
    if (r.Method=="POST") {
        // Save indicator's values
        r.ParseForm()
        var datas Datas
        // Select file for this specific indicator
        if (indicator!="") {
            fmt.Println(indicator)
            var found bool
            var filename string
            found = false
            // Read indicators list
            var settings Settings
            settings.Read("./datas/main.json", password)
            for _, value := range settings.List {
                fmt.Println(value.Slug)
                if (value.Slug == indicator) {
                    found = true
                    filename = value.Filename
                    break
                }
            }
            if (!found) {
                // Create new filename
                fmt.Println("Not found")
                var indic_file IndicatorFile
                indic_file.Slug = indicator
                indic_file.Filename = randString(32) + ".json"
                filename = indic_file.Filename
                // TODO: be sure this file is not existing already
                settings.List = append(settings.List, indic_file)
                settings.Write("./datas/main.json", password)
                // Create file
                datas.Write("./datas/"+filename, password)
            }
            fmt.Println(filename)
            // Open filename and use it if necessary
            var save bool
            save = false
            for k, v := range r.Form {
                if (k!="indicator" && k!="datetime") {
                    if (v[0]!="") {
                        //fmt.Println("Save something !" + v[0])
                        save = true
                        break
                    }
                }
            }
            if (save) {
                datas.Read("./datas/"+filename, password)
                var new_data Data
                var new_val Value
                for k, v := range r.Form {
                    switch (k) {
                        case "indicator":
                        case "notes":
                            new_data.Notes = v[0]
                        case "datetime":
                            new_data.Timestamp = v[0]
                        default:
                            new_val.Slug = k
                            new_val.Value = v[0]
                            new_data.Values = append(new_data.Values, new_val)
                    }
                }
                datas.Datas = append(datas.Datas, new_data)
                datas.Write("./datas/"+filename, password)
                fmt.Println("Filename wrote!")
            }
        }
    }

    // Display indicator
    var indics Indicators
    indics.Read("./datas/indicators.json")
    for _, value := range indics.List {
        if (value.Slug == indicator) {
            // Creating context struct
            context := struct {
                Indicator Indicator
                Now string
            } {
                value,
                time.Now().Format("2006-01-02 15:04:05"),
            }
            // Display indicator
            renderTemplate(w, "indicator", context)
            break
        }
    }
}


/************************************/
// Web utilities
/************************************/

/*
This function verifies password is set.
If password is not set
    - it redirects user to homepage
*/
func verifyPassword(w http.ResponseWriter, r *http.Request) {
    if len(password)<1 {
        http.Redirect(w, r, "/", 303)
    }
}

/*
This function renders a specific template based on its name, using the context informations and write it to the ResponseWriter.
*/
func renderTemplate(w http.ResponseWriter, tmpl string, context interface{}) {
    err := templates.ExecuteTemplate(w, tmpl+".html", context)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }
}

/*
This function initialize routes for the web application.
*/
func prepareHandlers() *mux.Router {
    // Preparing locales
    setup("fr_FR", "messages", "locale")
    //TODO: This part should be dynamic !!!
    // Default language is en_US. This will change after user's parameters are loaded
    changeLocale("en_US")
    // TODO: Use parameters to set user's language
    changeLocale("fr_FR")

    r := mux.NewRouter()
    // Routes of application
    // Homepage
    r.HandleFunc("/", homeHandler).Methods("GET")
    // Set of password ad creation of main.json
    r.HandleFunc("/create", createHandler).Methods("POST")
    // Login page
    r.HandleFunc("/login", loginHandler).Methods("GET", "POST")
    // Logout page
    r.HandleFunc("/logout", logoutHandler).Methods("GET")
    // Personal informations page
    r.HandleFunc("/personal", personalHandler).Methods("GET", "POST")
    // Parameters page
    r.HandleFunc("/parameters", parametersHandler).Methods("GET")
    // Dashboard page
    r.HandleFunc("/dashboard", dashboardHandler).Methods("GET")
    // Public page
    r.HandleFunc("/public", publicHandler).Methods("GET")
    // Menu page
    r.HandleFunc("/menu", menuHandler).Methods("GET")
    // All categories page
    r.HandleFunc("/category", categoryHandler).Methods("GET")
    // Specific category page
    r.HandleFunc("/category/{category}", categoryHandler).Methods("GET", "POST")
    // Specific indicator page
    r.HandleFunc("/indicator/{indicator}", indicatorHandler).Methods("GET","POST")
    // Specific path for static files
    r.PathPrefix("/static").Handler(http.StripPrefix("/static", http.FileServer(http.Dir("static/")))) 
    return r
}
