# Life Monitor Application


### Compilation

Go in lifemon folder, install some dependencies :

    go get github.com/gorilla/mux
    go get github.com/chai2010/gettext-go/gettext

Compile entire module

    go build

Run the compiled software

    ./lifemon (on GNU/Linux systems)
    lifemon.exe (on Windows systems)

View the aplication on a browser

    http://localhost:8080/


### Localization

Lifemon uses the gettext library, but there are some strings in html templates and in json files. So
localization has to be prepared :

Retrieve strings to translate (each time some new strings are added)

    sed "s/{{gt \(".*"\)}}/{{gettext(\1)}}/g" templates/*.html | xgettext --no-wrap --language=c --from-code=UTF-8 --output=locale/templates.pot -
    sed "s/\"slug\": \(.*\)/\"slug\": gettext(\1)/g" datas/categories.json | xgettext --no-wrap --language=c --from-code=UTF-8 --output=locale/categories.pot -

Create the final translation file (each time some new strings are added)

    xgettext --files-from=locale/potfiles --directory=. --output=locale/messages.pot

Create a file for each language (to do once)

    msginit --no-wrap --no-translator --input=locale/messages.pot --locale=en_US -o locale/en_US/LC_MESSAGES/messages.po 
    msginit --no-wrap --no-translator --input=locale/messages.pot --locale=fr_FR -o locale/fr_FR/LC_MESSAGES/messages.po 
    do the same for each language...

Update files for each language (each time some new strings are added)

    msgmerge --no-wrap --backup=none --update locale/en_US/LC_MESSAGES/messages.po locale/messages.pot
    msgmerge --no-wrap --backup=none --update locale/fr_FR/LC_MESSAGES/messages.po locale/messages.pot
    do the same for each language...

Generate .mo files for each language (each time some new strings are added)

    msgfmt locale/en_US/LC_MESSAGES/messages.po -o locale/en_US/LC_MESSAGES/messages.mo
    msgfmt locale/fr_FR/LC_MESSAGES/messages.po -o locale/fr_FR/LC_MESSAGES/messages.mo
    do the same for each language...

The simplest way is to use the _translate.sh_ script for this
