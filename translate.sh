#!/bin/sh

# Retrieve strings to translate
sed "s/{{gt \(".*"\)}}/{{gettext(\1)}}/g" templates/*.html | xgettext --no-wrap --language=c --from-code=UTF-8 --output=locale/templates.pot -
sed "s/\"name\": \(.*\)/\"name\":gettext(\1)/g" datas/categories.json | xgettext --no-wrap --language=c --from-code=UTF-8 --output=locale/categories.pot -
sed "s/\"\(name\|label\|description\)\": \(\".*\"\)/\"\1\": gettext(\2)/g" datas/indicators.json | xgettext --no-wrap --language=c --from-code=UTF-8 --output=locale/indicators.pot -

# Create the final translation file
xgettext --files-from=locale/potfiles --directory=. --output=locale/messages.pot

# Create a file for each language
#msginit --no-wrap --no-translator --input=locale/messages.pot --locale=en_US -o locale/en_US/LC_MESSAGES/messages.po 
#msginit --no-wrap --no-translator --input=locale/messages.pot --locale=fr_FR -o locale/fr_FR/LC_MESSAGES/messages.po 

# Update file for each language
msgmerge --no-wrap --backup=none --update locale/en_US/LC_MESSAGES/messages.po locale/messages.pot
msgmerge --no-wrap --backup=none --update locale/fr_FR/LC_MESSAGES/messages.po locale/messages.pot

# Generate .mo files
msgfmt locale/en_US/LC_MESSAGES/messages.po -o locale/en_US/LC_MESSAGES/messages.mo
msgfmt locale/fr_FR/LC_MESSAGES/messages.po -o locale/fr_FR/LC_MESSAGES/messages.mo
